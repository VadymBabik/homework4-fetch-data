"use strict";

const GHE = teg => document.querySelector(teg);
const CHE = elem => document.createElement(elem);

function listFilms(url, elem) {
    fetch(url)
        .then((parsJson) => parsJson.json())
        .then((data) => data.results)
        .then((films) => {
            const tag = GHE(elem);
            const ul = document.createElement("ul");
            ul.style = "list-style-type: none";

            films.forEach((film) => {
                const items = CHE("li");
                items.innerHTML = `
					<h2><u> ${film.title}</u></h2>
					<H4>movie episode: ${film.episode_id}</H4>
					<p>${film.opening_crawl}</p>
					`;
                const listChar = CHE("ul");
                listChar.style = "list-style-type: none"
                const charactersArr = film.characters;
                const fetchArr = charactersArr.map((item) => {
                    return fetch(item)
                        .then((resJson) => resJson.json())
                        .then((charName) => charName.name);
                });
                Promise.all(fetchArr).then((data) => {
                    data.forEach((elem) => {
                        const items = CHE("li");
                        items.innerHTML = `${elem}`;
                        listChar.append(items);
                    });
                })

                items.append(listChar);
                ul.append(items);

                return tag.prepend(ul);
            });
        });
}


listFilms("https://swapi.dev/api/films/", "body");
